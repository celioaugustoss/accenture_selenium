package support;

import java.io.IOException;

public class TestFixture {
    public SeleniumHelper BrowserHelper;
    public ConfigurationHelper Configuration;
    public Browser Browser;

    public TestFixture() throws IOException {
        Configuration = new ConfigurationHelper();
        BrowserHelper = new SeleniumHelper(Browser.valueOf(Configuration.getPropValues("Browser")), Configuration, false);

    }
}
