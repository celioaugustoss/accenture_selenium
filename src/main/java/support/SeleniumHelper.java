package support;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.Select;


import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.ThreadLocalRandom;

public class SeleniumHelper {
    public WebDriver Webdriver;
    public WebDriverWait Wait;
    public ConfigurationHelper Configuration;

    public SeleniumHelper(Browser browser,
                          ConfigurationHelper configuration,
                          boolean headless) throws IOException {
        Configuration = configuration;
        Webdriver = BrowserConfig.CriarWebDriver(browser, headless);
        Webdriver.manage().window().maximize();
        Wait = new WebDriverWait(Webdriver, Duration.ofSeconds(10));

    }


    public void IrParaUrl() throws IOException {

        Webdriver.get(Configuration.getPropValues("WebSite"));
    }
    public void PreencherPorTexto(By locator, String texto){
        WebElement campo;
        campo = Wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        campo.sendKeys(texto);
    }
    
    public void Upload(By locator, String file) {
     
    	ClassLoader classLoader = getClass().getClassLoader();
    	 
        File file1 = new File(classLoader.getResource("borra.jpg").getFile());
        
    	Webdriver.findElement(By.id("picturecontainer")).sendKeys(file1.getPath());
    	
    }  
    
    public void ScrollPage() {
    	WebElement element = Webdriver.findElement(By.tagName("header"));

    	JavascriptExecutor js = (JavascriptExecutor)Webdriver;
    	js.executeScript("arguments[0].scrollIntoView();", element); 
    }
    
    public String ObterValorTextBox(By locator){
        String text;
        text = Wait.until(ExpectedConditions.visibilityOfElementLocated(locator)).getAttribute("value");
        return text;
    }

    public String ObterValorTexto(By locator){
        return  Wait.until(ExpectedConditions.visibilityOfElementLocated(locator)).getText();
    }

    public void SelecionarPorTexto(By locator, String item){
        WebElement campo = Wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        Select selectElement = new Select(campo);
        selectElement.selectByVisibleText(item);
    }
    
    public void SelecionarRandOptions(By locator) {
    	WebElement campoSelect = Wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    	Select  selectElement = new Select(campoSelect); 
    	List<WebElement> itensDropDown = selectElement.getOptions();
    	int size = itensDropDown.size();
    	int randNumber = ThreadLocalRandom.current().nextInt(1, size);    	
    	selectElement.selectByIndex(randNumber);
    }

    public void Screeshot(String evidencia) throws IOException {

        Date date = new Date();

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM HH-mm");
        TakesScreenshot scrShot =((TakesScreenshot)Webdriver);
        File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(SrcFile,new File (Configuration.getPropValues("Folder_Evidencias") + evidencia + " " + sdf.format(date) +".png"));

    }
     public boolean ValidarElementoExiste(By locator){
        return ElementoExiste(locator);
     }
     public boolean ElementoExiste(By locator){
        try{
            Webdriver.findElement(locator);
            return true;
        }catch (NoSuchElementException e){
            return false;
        }
     }

    public void Clicar(By locator){
        WebElement button;
        button = Wait.until(ExpectedConditions.elementToBeClickable(locator));
        button.click();
    }

    public String ObterUrl(){
       return Webdriver.getCurrentUrl();
    }

    public void Dispose(){
        Webdriver.quit();
    }

}
