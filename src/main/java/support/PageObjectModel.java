package support;

import java.io.IOException;

public abstract class PageObjectModel {
    protected SeleniumHelper Helper;
    public PageObjectModel(SeleniumHelper helper){
        Helper = helper;
    }

    public String ObterUrl(){
        return Helper.ObterUrl();
    }
    
    public void Screeshot(String evidencia) throws IOException {
    	Helper.Screeshot(evidencia);
    }



}
