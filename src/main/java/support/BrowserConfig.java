package support;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

public class BrowserConfig {

    public static WebDriver CriarWebDriver(Browser browser, boolean headless) throws IOException {

        ConfigurationHelper configuration = new ConfigurationHelper();
        WebDriver webDriver = null;
       
        switch (browser){
            case Chrome:
                System.setProperty("webdriver.chrome.driver", configuration.getPropValues("PathDriver"));
                ChromeOptions chromeOptions = new ChromeOptions();
                if (headless){
                    chromeOptions.addArguments("--headless");
                }
                webDriver = new ChromeDriver(chromeOptions);
                break;
            case Firefox:
                System.setProperty("webdriver.gecko.driver", configuration.getPropValues("PathDriverFireFox"));
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                if (headless){
                    firefoxOptions.addArguments("--headless");
                }
                webDriver = new FirefoxDriver(firefoxOptions);
                break;
        }
        return webDriver;
    }

}