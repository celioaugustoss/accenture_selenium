package PageObjects;

import org.openqa.selenium.By;

import support.PageObjectModel;
import support.SeleniumHelper;

public class Pgo_PriceOption extends PageObjectModel{
	
	public Pgo_PriceOption(SeleniumHelper helper) {
		super(helper);
		
	}
	
	public By lblSilver = By.xpath("//th[normalize-space()='Silver']");
	public By lblGold = By.xpath("//th[normalize-space()='Gold']");
	public By lblPlatinum = By.xpath("//th[normalize-space()='Platinum']");
	public By lblUltimate = By.xpath("//th[normalize-space()='Ultimate']");
	public By viewQuote = By.cssSelector("a[id='viewquote'] i[class='fa fa-file-pdf-o']");
	public By downloadQuote = By.cssSelector("a[id='downloadquote'] i[class='fa fa-file-pdf-o']");
	
	public By tdPrice = By.xpath("//td[normalize-space()='Price per Year ($)']");
	public By tdOnlineClaim = By.xpath("//td[normalize-space()='Online Claim']");
	public By tdClaimDiscount = By.xpath("//td[normalize-space()='Claims Discount (%)']");
	public By tdWorldwide = By.xpath("//td[normalize-space()='Worldwide Cover']");
	public By btnNextQuote = By.cssSelector("#nextsendquote");
    
	
	public void SendQuote() {
    	Helper.Clicar(btnNextQuote);
    }
	
	public void SelecionarPlano(String option) {
		
		switch(option) {
		case "Silver":
			Helper.Clicar(By.xpath("//table[@id='priceTable']/tfoot/tr/th[2]/label/span"));
			break;
		case "Gold":
			Helper.Clicar(By.xpath("//table[@id='priceTable']/tfoot/tr/th[2]/label[2]/span"));
			break;
		case "Platinum":
			Helper.Clicar(By.xpath("//table[@id='priceTable']/tfoot/tr/th[2]/label[3]/span"));
			break;
		case "Ultimate":
			Helper.Clicar(By.xpath("//table[@id='priceTable']/tfoot/tr/th[2]/label[4]/span"));
			break;
		 default:
			    System.out.println("Plano não encontrado");	
		}
		
	
	}
}
