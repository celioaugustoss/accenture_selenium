package PageObjects;

import org.openqa.selenium.By;

import support.PageObjectModel;
import support.SeleniumHelper;

public class Pgo_SendQuote extends PageObjectModel{
	
	public Pgo_SendQuote(SeleniumHelper helper) {
		super(helper);
		}
		
		public By cpEmail = By.cssSelector("#email");
		public By cpPhone = By.cssSelector("#phone");
		public By cpUsername = By.cssSelector("#username");
		public By cpPassword = By.cssSelector("#password");
		public By cpConfirmPassword = By.cssSelector("#confirmpassword");
		public By cpComments = By.cssSelector("#Comments");
		
		public By btnPrevPriceOption = By.cssSelector("#prevselectpriceoption");
		public By btnSend = By.cssSelector("#sendemail");
		
		public By btnMainPage = By.cssSelector(".fa.fa-home");
		public By btnNewTruck = By.cssSelector(".fa.fa-truck");
		public By btnNewCamper = By.cssSelector(".fa.fa-bus");
		public By btnNewAuto = By.cssSelector(".fa.fa-car");
		public By btnNewMoto = By.cssSelector(".fa.fa-motorcycle");
		
		
		public void Send_Quote() {
			Helper.Clicar(btnSend);
		}
		
		public void PreencherEmail(String email) {
			Helper.PreencherPorTexto(cpEmail, email);
		}
		
		public void PreencherPhone(String phone) {
			Helper.PreencherPorTexto(cpPhone, phone);
		}
		
		public void PreencherUsername(String username) {
			Helper.PreencherPorTexto(cpUsername, username);
		}
		
		public void PreencherPassword(String password) {
			Helper.PreencherPorTexto(cpPassword, password);
		}
		
		public void PreencherConfirmPassword(String confirm) {
			Helper.PreencherPorTexto(cpConfirmPassword, confirm);
		}
		
		public void PreencherComments(String comments) {
			Helper.PreencherPorTexto(cpComments, comments);
		}
		
		public void PreencherSendQuote(SendQuote sendQuote) {
			PreencherEmail(sendQuote.getEmail());
			PreencherPhone("911154899");
			PreencherUsername(sendQuote.getUsername());
			PreencherPassword(sendQuote.getPassword());
			PreencherConfirmPassword(sendQuote.getConfirmPassword());
			PreencherComments(sendQuote.getComments());
			
		}
	
}
