package PageObjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;

public class Insurant {
	
	private String firstName;
	private String lastName;
	private String dateBirth;
	private String gender;	
	private String streetAddress;
	private String country;
	private String zipCode;
	private String city;
	private String occupation;
	private String webSite;
	
	public Insurant() {}
	
	public Insurant(String firstName, String lastName, String dateBirth, String gender, String streetAddress,
			String country, String zipCode, String city, String occupation, String webSite) {
		
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateBirth = dateBirth;
		this.gender = gender;
		this.streetAddress = streetAddress;
		this.country = country;
		this.zipCode = zipCode;
		this.city = city;
		this.occupation = occupation;
		this.webSite = webSite;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDateBirth() {
		return dateBirth;
	}
	public void setDateBirth(String dateBirth) {
		this.dateBirth = dateBirth;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getStreetAddress() {
		return streetAddress;
	}
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}
	
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getWebSite() {
		return webSite;
	}
	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}
	
	public List<String> getOccupationList(){
		List<String> occupationList = new ArrayList<>();
		occupationList.add("Employee");
        occupationList.add("Public Official");
        occupationList.add("Farmer");
        occupationList.add("Unemployed");
        occupationList.add("Selfemployed");
		return occupationList;	
		
	}
	
	public String randomOccupationList() {
		Random r = new Random();
	    int randomitem = r.nextInt(getOccupationList().size());
	    String randomElement = getOccupationList().get(randomitem);
	    return randomElement;
	}

}
