package PageObjects;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.github.javafaker.DateAndTime;

public class Vehicle {
	
	private int cylinderCapacity;
	private int enginePerfomance;
	private String dateOfManufacture; //MM/DD/YYYY
	private int numberOfSeats;
	private Boolean rightHandDrive;
	private int payload;
	private int totalWeight;
	private int listPrice;
	private String licensePlateNumber;
	private int annualMileage;
	
	public Vehicle() {}
	
	public Vehicle(int cylinderCapacity, int enginePerfomance, String dateOfManufacture,
			int numberOfSeats, Boolean rightHandDrive, int payload, int totalWeight, int listPrice,
			String licensePlateNumber, int annualMileage) {		
		
		this.cylinderCapacity = cylinderCapacity;
		this.enginePerfomance = enginePerfomance;
		this.dateOfManufacture = dateOfManufacture;
		this.numberOfSeats = numberOfSeats;
		this.rightHandDrive = rightHandDrive;
		this.payload = payload;
		this.totalWeight = totalWeight;
		this.listPrice = listPrice;
		this.licensePlateNumber = licensePlateNumber;
		this.annualMileage = annualMileage;
	}
	
	public int getCylinderCapacity() {
		return cylinderCapacity;
	}


	public void setCylinderCapacity(int cylinderCapacity) {
		this.cylinderCapacity = cylinderCapacity;
	}


	public int getEnginePerfomance() {
		return enginePerfomance;
	}


	public void setEnginePerfomance(int enginePerfomance) {
		this.enginePerfomance = enginePerfomance;
	}


	public String getDateOfManufacture() {
		return dateOfManufacture;
	}


	public void setDateOfManufacture(String dateAndTime) {
		this.dateOfManufacture = dateAndTime;
	}


	public int getNumberOfSeats() {
		return numberOfSeats;
	}


	public void setNumberOfSeats(int numberOfSeats) {
		this.numberOfSeats = numberOfSeats;
	}


	public Boolean getRightHandDrive() {
		return rightHandDrive;
	}


	public void setRightHandDrive(Boolean rightHandDrive) {
		this.rightHandDrive = rightHandDrive;
	}


	public int getPayload() {
		return payload;
	}


	public void setPayload(int payload) {
		this.payload = payload;
	}


	public int getTotalWeight() {
		return totalWeight;
	}


	public void setTotalWeight(int totalWeight) {
		this.totalWeight = totalWeight;
	}


	public int getListPrice() {
		return listPrice;
	}


	public void setListPrice(int listPrice) {
		this.listPrice = listPrice;
	}


	public String getLicensePlateNumber() {
		return licensePlateNumber;
	}


	public void setLicensePlateNumber(String licensePlateNumber) {
		this.licensePlateNumber = licensePlateNumber;
	}


	public int getAnnualMileage() {
		return annualMileage;
	}


	public void setAnnualMileage(int annualMileage) {
		this.annualMileage = annualMileage;
	}
	
	
	public List<String> getMakelList(){
		List<String> makeList = new ArrayList<>();
		makeList.add("Audi");
        makeList.add("BMW");                                            
        makeList.add("Ford");
        makeList.add("Honda"); 
        makeList.add("Mazda");
        makeList.add("Mercedes Benz"); 
        makeList.add("Nissan");
        makeList.add("Opel");
        makeList.add("Porsche"); 
        makeList.add("Renault");
        makeList.add("Skoda");
        makeList.add("Suzuki"); 
        makeList.add("Toyota"); 
        makeList.add("Volkswagen");
        makeList.add("Volvo");  
		return makeList;
		
	}
	
	public List<String> getModelList(){
		List<String> modelList = new ArrayList<>();
		modelList.add("Scooter");
        modelList.add("Three-Wheeler");
        modelList.add("Moped");
        modelList.add("Motorcycle");
		return modelList;	
		
	}
	
	public String randomModelList() {
		 Random r = new Random();
		    int randomitem = r.nextInt(getModelList().size());
		    String randomElement = getModelList().get(randomitem);
		    return randomElement;
	}
	
	public String randomMakeList() {
		Random r = new Random();
	    int randomitem = r.nextInt(getMakelList().size());
	    String randomElement = getMakelList().get(randomitem);
	    return randomElement;
	}
}
