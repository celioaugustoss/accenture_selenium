package PageObjects;



import support.PageObjectModel;
import support.SeleniumHelper;
import java.io.IOException;
import org.openqa.selenium.By;
import support.PageObjectModel;
import support.SeleniumHelper;


public class Pgo_Product_Data extends PageObjectModel{
	
	
	public Pgo_Product_Data(SeleniumHelper helper) {
		super(helper);
		
	}
	
	
	public By cpStartDate = By.cssSelector("#startdate");
	public By sltInsuranceSum = By.cssSelector("#insurancesum");
	public By sltMeritRating = By.cssSelector("#meritrating");
	public By sltDamageInsurance = By.cssSelector("#damageinsurance");
	public By rdOptionalProducts_Euro = By.xpath("//label[normalize-space()='Euro Protection']");
	public By rdOptionalProducts_Legal = By.xpath("//label[normalize-space()='Legal Defense Insurance']");
	public By sltCourtesyCar = By.cssSelector("#courtesycar");
	public By btnPrev = By.cssSelector("#preventerinsurancedata");
	public By btnNext = By.cssSelector("#nextselectpriceoption");
	
	
	public void PreencherStartDate(By locator, String data) {
		Helper.PreencherPorTexto(cpStartDate, data);
	}
	
	public void SelecionarInsuranceSum(By locator) {
		Helper.SelecionarRandOptions(sltInsuranceSum);
	}
	
	public void SelecionarMeritRating(By locator) {
		Helper.SelecionarRandOptions(sltMeritRating);
	}
	
	public void SelecionarDamegeInsurance(By locator) {
		Helper.SelecionarRandOptions(sltDamageInsurance);
	}
	
	
	public void SelecionarCourtesyCar(By locator) {
		Helper.SelecionarRandOptions(sltCourtesyCar);
	}
	
	public void NextPrice() {
		Helper.Clicar(btnNext);
	}
	
	public void PreencherProduct(Product product) {
		PreencherStartDate(cpStartDate, product.startDate);
		SelecionarInsuranceSum(sltInsuranceSum);
		SelecionarMeritRating(sltMeritRating);
		SelecionarDamegeInsurance(sltDamageInsurance);
		Helper.Clicar(rdOptionalProducts_Euro);
		SelecionarCourtesyCar(sltCourtesyCar);		
		
	}
	

}
