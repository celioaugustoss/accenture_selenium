package PageObjects;

import java.io.IOException;
import org.openqa.selenium.By;
import support.PageObjectModel;
import support.SeleniumHelper;
//import org.junit.Assert;


public class Pgo_Vehicle_Data extends PageObjectModel{

	
	public By sltMake = By.cssSelector("#make");
	public By sltModel = By.cssSelector("#model");
	public By cpCylindeCapacity = By.cssSelector("#cylindercapacity");
	public By cpEnginePerformance = By.cssSelector("#engineperformance");
	public By cpDate = By.cssSelector("#dateofmanufacture");
	public By slNumberSeats = By.cssSelector("#numberofseats");
	public By rdSim = By.xpath("//label[normalize-space()='Yes']");
	public By rdNao = By.xpath("//label[normalize-space()='No']");
	public By slNumberSeats_2 = By.cssSelector("#numberofseatsmotorcycle");
	public By slFuelType = By.cssSelector("#fuel");
	public By cpPayload = By.cssSelector("#payload");
	public By cpTotalWeight = By.cssSelector("#totalweight");
	public By cpListPrice  = By.cssSelector("#listprice");
	public By cpLicensePlateNumber = By.cssSelector("#licenseplatenumber");
	public By cpAnnualMileage = By.cssSelector("#annualmileage");
	public By btnNext = By.cssSelector("#nextenterinsurantdata");
	
	
	
	
	
	public Pgo_Vehicle_Data(SeleniumHelper helper) {
		super(helper);	
		
	}
	
	public void PreencherCilindrada(Integer cilindrada) {
		Helper.PreencherPorTexto(cpCylindeCapacity, String.valueOf(cilindrada));
	}
	
	public void SelecionarTipo(String tipo) {
		Helper.SelecionarPorTexto(sltModel, tipo);
	}
	
	public void SelecionarModelo(String modelo) {		
		Helper.SelecionarPorTexto(sltMake, modelo);		
		
	}
	
	public void Select(Vehicle vehicle) {
		Helper.PreencherPorTexto(sltModel, vehicle.randomModelList());
	}
	public void PreencherPerfomace(String perfomance) {
		Helper.PreencherPorTexto(cpEnginePerformance, String.valueOf(perfomance));
	}
	 
	public void PreencherData(String date) {
		Helper.PreencherPorTexto(cpDate, String.valueOf(date));
	}
	public void SelecionarNumberSeats(Integer numberSeats) {
		Helper.SelecionarPorTexto(slNumberSeats, String.valueOf(numberSeats));
	}
	
	public void ClicarOpcaoHandDrive(String opcao) {
		if(opcao.equals("Yes") || opcao.equals("yes")) {
			Helper.Clicar(rdSim);
		}if(opcao.equals("Não") || opcao.equals("não")) {
			Helper.Clicar(rdNao);
		}
	}
	
	public void SelecionarNumberSeats_2(Integer numberSeats_2) {
		Helper.SelecionarPorTexto(slNumberSeats_2, String.valueOf(numberSeats_2));
	}
	
	public void SelecionarFuel(String fuel) {
		Helper.SelecionarPorTexto(slFuelType, fuel);
	}
	
	
	public void PreencherPayload(Integer payload) {
		Helper.PreencherPorTexto(cpPayload, String.valueOf(payload));
	}
	public void PreencherTotalWeight(Integer totalWeight) {
		Helper.PreencherPorTexto(cpTotalWeight, String.valueOf(totalWeight));
	}
	public void PreencherListPrice(Integer listPrice) {
		Helper.PreencherPorTexto(cpListPrice, String.valueOf(listPrice));
	}
	public void PreencherLicensePlateNumber(String licensePlateNumber) {
		Helper.PreencherPorTexto(cpLicensePlateNumber, licensePlateNumber);
	}
	public void PreencherAnnualMileage(Integer annualMileage) {
		Helper.PreencherPorTexto(cpAnnualMileage, String.valueOf(annualMileage));
	}
	
	public void ClicarNext() {
		Helper.Clicar(btnNext);
		
	}
	
	
	public void PreencherVehicle(Vehicle vehicle) {
		SelecionarModelo(vehicle.randomMakeList());
		SelecionarTipo(vehicle.randomModelList());
		PreencherCilindrada(vehicle.getCylinderCapacity());
		PreencherPerfomace(String.valueOf(vehicle.getEnginePerfomance()));
		PreencherData(String.valueOf(vehicle.getDateOfManufacture()));
		Helper.SelecionarRandOptions(slNumberSeats);
		Helper.Clicar(rdSim);
		Helper.SelecionarRandOptions(slNumberSeats_2);
		Helper.SelecionarRandOptions(slFuelType);
		Helper.PreencherPorTexto(cpPayload, String.valueOf(vehicle.getPayload()));
		Helper.PreencherPorTexto(cpTotalWeight, String.valueOf(vehicle.getTotalWeight()));
		Helper.PreencherPorTexto(cpListPrice, String.valueOf(vehicle.getListPrice()));
		Helper.PreencherPorTexto(cpLicensePlateNumber, String.valueOf(vehicle.getLicensePlateNumber()));
		Helper.PreencherPorTexto(cpAnnualMileage, String.valueOf(vehicle.getAnnualMileage()));
	

	}

}
