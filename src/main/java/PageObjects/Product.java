package PageObjects;

public class Product {
	
	public String startDate;
	public String insuranceSum;
	public String meritRating;
	public String damageInsurance;
	public String optionalProducts;
	public String courtesyCar;
	
	
	public Product() {}	
	
	public Product(String startDate) {
		this.startDate = startDate;
	}

	public Product(String startDate, String insuranceSum, String meritRating, String damageInsurance,
			String optionalProducts, String courtesyCar) {
		super();
		this.startDate = startDate;
		this.insuranceSum = insuranceSum;
		this.meritRating = meritRating;
		this.damageInsurance = damageInsurance;
		this.optionalProducts = optionalProducts;
		this.courtesyCar = courtesyCar;
	}



	//Getters and Setters
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getInsuranceSum() {
		return insuranceSum;
	}
	public void setInsuranceSum(String insuranceSum) {
		this.insuranceSum = insuranceSum;
	}
	public String getMeritRating() {
		return meritRating;
	}
	public void setMeritRating(String meritRating) {
		this.meritRating = meritRating;
	}
	public String getDamageInsurance() {
		return damageInsurance;
	}
	public void setDamageInsurance(String damageInsurance) {
		this.damageInsurance = damageInsurance;
	}
	public String getOptionalProducts() {
		return optionalProducts;
	}
	public void setOptionalProducts(String optionalProducts) {
		this.optionalProducts = optionalProducts;
	}
	public String getCourtesyCar() {
		return courtesyCar;
	}
	public void setCourtesyCar(String courtesyCar) {
		this.courtesyCar = courtesyCar;
	}

}
