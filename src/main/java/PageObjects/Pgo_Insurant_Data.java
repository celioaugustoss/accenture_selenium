package PageObjects;

import support.PageObjectModel;
import support.SeleniumHelper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import support.PageObjectModel;
import support.SeleniumHelper;
//import org.junit.Assert;

public class Pgo_Insurant_Data extends PageObjectModel{
	
	public By cpFirstName = By.cssSelector("#firstname");
	public By cpLastName = By.cssSelector("#lastname");
	public By cpDateBirth = By.cssSelector("#birthdate");
	public By rdMale = By.xpath("//label[normalize-space()='Male']");
	public By rdFemale = By.xpath("//label[normalize-space()='Female']");
	public By cpStreetAddress = By.cssSelector("#streetaddress");
	public By slCountry = By.cssSelector("#country");
	public By cpZipCode = By.cssSelector("#zipcode");
	public By cpCity = By.cssSelector("#city");
	public By slOccupation = By.cssSelector("#occupation");
	public By cpWebSite = By.cssSelector("#website");
	public By cpPicture = By.xpath("//input[@id='picturecontainer']");
	public By btnNextProduct = By.cssSelector("#nextenterproductdata");
	

	public Pgo_Insurant_Data(SeleniumHelper helper) {
		super(helper);
		
	}
	
	public void PreencherFirstName(String firstName) {
		Helper.PreencherPorTexto(cpFirstName, firstName);
	}
	
	public void PreencherLastName(String lastName) {
		Helper.PreencherPorTexto(cpLastName, lastName);
	}
	
	public void PreencherDateBirth(String date) {
		Helper.PreencherPorTexto(cpDateBirth, String.valueOf(date));
	}
	
	public void EscolherGenero(String gender) {
		if(gender.equals("Male")) {
			Helper.Clicar(rdMale);
		}if(gender.equals("Female")) {
			Helper.Clicar(rdFemale);
		}
	}
	public void PreencherStreetAddress(String address) {
		Helper.PreencherPorTexto(cpStreetAddress, address);
	}
	
	public void SelecionarCountry(String country) {		
		Helper.SelecionarPorTexto(slCountry, country);		
		
	}
	
	public void PreencherZipCode(String zipCode) {
		Helper.PreencherPorTexto(cpZipCode, zipCode);
	}
	
	public void PreencherCity(String city) {
		Helper.PreencherPorTexto(cpCity, city);
	}
	
	public void SelecionarOccupation(String occupation) {		
		Helper.SelecionarPorTexto(slOccupation, occupation);		
		
	}
	
	
	public void PreencherWebsite(String webSite) {
		Helper.PreencherPorTexto(cpWebSite, String.valueOf(webSite));
	}
	public void PreencherPicture(String picture) {
		Helper.PreencherPorTexto(cpDateBirth, String.valueOf(picture));
	}
	
	public void SelecionarHobbies() {
	
		Hobbies arr[] = Hobbies.values();	
	
		for(Hobbies hobby : arr) {
			if(hobby.toString().contains("_")) {
				Helper.Clicar(By.xpath("//label[normalize-space()='" + hobby.toString().replace("_", " ") +"']"));
			}
			else {
				Helper.Clicar(By.xpath("//label[normalize-space()='" + hobby +"']"));
			}
			
		}
	}
	
	
	public void PreencherInsurant(Insurant insurant) {
		PreencherFirstName(insurant.getFirstName());
		PreencherLastName(insurant.getLastName());
		PreencherDateBirth("08/07/2000");
		EscolherGenero("Female");
		PreencherStreetAddress(insurant.getStreetAddress());
		SelecionarCountry("United States");
		PreencherZipCode("0599805");
		PreencherCity(insurant.getCity());
		Helper.SelecionarRandOptions(slOccupation);
		SelecionarHobbies();
		PreencherWebsite(insurant.getWebSite());
		Helper.Upload(cpPicture, "Captura11r.JPG");
	
	}
	
	public void NextProduct() {
		Helper.Clicar(btnNextProduct);
	}
	
	private enum Hobbies{
		Speeding,
		Bungee_Jumping,
		Cliff_Diving,
		Skydiving,
		Other
	}
	

}
