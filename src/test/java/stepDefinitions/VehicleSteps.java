package stepDefinitions;
import java.io.IOException;
import java.util.Locale;

import org.openqa.selenium.By;

import com.github.javafaker.Faker;

import PageObjects.Insurant;
import PageObjects.Pgo_Insurant_Data;
import PageObjects.Pgo_PriceOption;
import PageObjects.Pgo_Product_Data;
import PageObjects.Pgo_SendQuote;
import PageObjects.Pgo_Vehicle_Data;
import PageObjects.Product;
import PageObjects.SendQuote;
import PageObjects.Vehicle;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import junit.framework.Assert;
import support.TestFixture;

public class VehicleSteps {
	private TestFixture _testFixture;
	private Pgo_Vehicle_Data vehicle_Data;
	private Vehicle vehicle;
	private Insurant insurant;
	private Pgo_Insurant_Data insurant_Data;
	private Pgo_Product_Data product_Data;
	private Product product;
	private Pgo_PriceOption priceOption;
	private Pgo_SendQuote sendQuote_Data;
	private SendQuote sendQuote;
	private Faker faker; 


public VehicleSteps(TestFixture testFixture) {
	_testFixture = testFixture;
	vehicle_Data = new Pgo_Vehicle_Data(testFixture.BrowserHelper);
	vehicle = new Vehicle();
	insurant_Data = new Pgo_Insurant_Data(testFixture.BrowserHelper);
	insurant = new Insurant();
	product_Data = new Pgo_Product_Data(testFixture.BrowserHelper);
	priceOption = new Pgo_PriceOption(testFixture.BrowserHelper);
	sendQuote_Data = new Pgo_SendQuote(testFixture.BrowserHelper);
	sendQuote = new SendQuote();
	product = new Product();
	faker = new Faker(new Locale("en-US"));
	
	
    
}
	@Given("Eu estou na tela de Seguro de automoveis") 
	public void eu_estou_na_tela_de_seguro_de_automoveis() throws IOException {
		//Assertion
		_testFixture.BrowserHelper.IrParaUrl();
		Assert.assertEquals(_testFixture.Configuration.getPropValues("WebSite"), vehicle_Data.ObterUrl());
		_testFixture.BrowserHelper.Screeshot("Aba_Dados_Veiculo");
	}

	
	
	@When("Eu preencher todos os dados do veiculo")
	public void eu_preencher_todos_os_dados_do_veiculo() throws IOException {		
		
		vehicle.setCylinderCapacity(faker.number().numberBetween(1, 2000));
		vehicle.setEnginePerfomance(faker.number().numberBetween(1, 2000));
		vehicle.setDateOfManufacture("01/01/2021");
		vehicle.setPayload(faker.number().numberBetween(1, 1000));
		vehicle.setTotalWeight(faker.number().numberBetween(1, 50000));
		vehicle.setListPrice(faker.number().numberBetween(500, 100000));
		vehicle.setLicensePlateNumber(faker.lorem().characters(1, 10, true, true));
		vehicle.setAnnualMileage(faker.number().numberBetween(100, 100000));
		vehicle_Data.PreencherVehicle(vehicle);
		_testFixture.BrowserHelper.Screeshot("Dados_Veiculo_preenchidos");
	}

	
	
	@Given("Eu estou na tela de dados de automovel")
	public void eu_estou_na_tela_de_dados_de_automovel() throws IOException {
	    
		_testFixture.BrowserHelper.IrParaUrl();
		Assert.assertEquals(_testFixture.Configuration.getPropValues("WebSite"), vehicle_Data.ObterUrl());
	}

	@When("Eu clicar em Next para dados do seguro")
	public void eu_clicar_em_next_para_dados_do_seguro() throws IOException {
	    // Write code here that turns the phrase above into concrete actions
		vehicle_Data.ClicarNext();
		_testFixture.BrowserHelper.Screeshot("Aba_Dados_Segurado");
	}

	@When("Eu preencher todos os daddos do segurado")
	public void eu_preencher_todos_os_daddos_do_segurado() throws IOException {
		
		insurant.setFirstName(faker.name().firstName());
		insurant.setLastName(faker.name().lastName());
		insurant.setStreetAddress(faker.address().streetAddress());
		insurant.setCountry(faker.address().country());
		insurant.setZipCode(faker.address().zipCode());
		insurant.setCity(faker.address().city());		
		insurant.setWebSite("www." + faker.name().firstName() + ".com");
		insurant_Data.PreencherInsurant(insurant);
		_testFixture.BrowserHelper.Screeshot("Dados_Segurado");
	   
	}

	@When("Eu clicar em next para produto")
	public void eu_clicar_em_next_para_produto() throws IOException {
		insurant_Data.NextProduct();
		_testFixture.BrowserHelper.Screeshot("Aba_Dados_Produto");
	}

	@When("Eu preencho todos os dados do produto")
	public void eu_preencho_todos_os_dados_do_produto() throws IOException {
	    product.setStartDate("08/31/2021");
	    product_Data.PreencherProduct(product);
	    _testFixture.BrowserHelper.Screeshot("Dados_Produto");
	    
	}

	@When("clico em next para selecionar o valor")
	public void clico_em_next_para_selecionar_o_valor() {
	    product_Data.NextPrice();
	}

	@When("Eu valido a cotacao")
	public void eu_valido_a_cotacao() throws IOException {
		
		Assert.assertEquals(_testFixture.BrowserHelper.ObterValorTexto(By.xpath("//td[@data-label='Ultimate'][normalize-space()='Submit']")), "Submit");
		Assert.assertEquals(_testFixture.BrowserHelper.ObterValorTexto(By.xpath("//td[contains(text(),'10')]")), "10");
		Assert.assertEquals(_testFixture.BrowserHelper.ObterValorTexto(By.xpath("//td[normalize-space()='Unlimited']")), "Unlimited");
		
		priceOption.SelecionarPlano("Ultimate");
		_testFixture.BrowserHelper.Screeshot("Aba_Prices");
		priceOption.SendQuote();
	}

	@When("preencho os dados para envio da cotacao")
	public void preencho_os_dados_para_envio_da_cotacao() throws IOException {
	    sendQuote.setEmail(faker.internet().emailAddress());
	    sendQuote.setPhone(faker.phoneNumber().cellPhone());
	    sendQuote.setUsername(faker.name().username());
	    sendQuote.setPassword("Teste@123");
	    sendQuote.setConfirmPassword("Teste@123");
	    sendQuote.setComments(faker.lorem().sentence(5));
	    
	    sendQuote_Data.PreencherSendQuote(sendQuote);
	    _testFixture.BrowserHelper.Screeshot("Dados_Envio_Cotacao");
	}

	@Then("Eu clico em send para enviar a cotacao")
	public void eu_clico_em_send_para_enviar_a_cotacao() throws IOException {
		sendQuote_Data.Send_Quote();
		
		Assert.assertEquals(_testFixture.BrowserHelper.ObterValorTexto(By.xpath("//h2[normalize-space()='Sending e-mail success!']")), "Sending e-mail success!");
		_testFixture.BrowserHelper.Screeshot("Confirmacao_Envio");
		_testFixture.BrowserHelper.Clicar(By.cssSelector(".confirm"));
		
	}
	
	
	
	@After
	public void Dispose() {
		_testFixture.BrowserHelper.Dispose();
	}
	
		
		




}
